<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'name' => 'Playstation 4',
            'slug' => 'playstation-4',
            'description' => 'PlayStation 4 – самая продаваемая консоль в мире. Доступны консоли в расцветках «Антрацитовый черный» и «Белый ледник», а также «Золотой» и «Серебряный» ограниченной серии.',
            'price' => 399.99,
            'image' => 'ps4.jpg',
        ]);

        DB::table('products')->insert([
            'name' => 'Xbox One',
            'slug' => 'xbox-one',
            'description' => 'Упивайся более насыщенными и яркими цветами в таких играх, как Gears of War 4 и Forza Horizon 3. Технология расширенного динамического диапазона повышает контраст между светлыми и темными участками изображения, представляя игры во всем их великолепии!',
            'price' => 449.99,
            'image' => 'xbox-one.jpg',
        ]);

        DB::table('products')->insert([
            'name' => 'Apple Macbook Pro',
            'slug' => 'macbook-pro',
            'description' => 'MacBook Pro стал быстрее и мощнее, но при этом он такой тонкий и лёгкий. А встроенная панель Touch Bar позволяет работать с Mac совершенно по-новому.',
            'price' => 2299.99,
            'image' => 'macbook-pro.jpg',
        ]);

        DB::table('products')->insert([
            'name' => 'Apple iPad Retina',
            'slug' => 'ipad-retina',
            'description' => 'iOS 11 в полной мере раскрывает способности iPad. Её новые функции значительно увеличивают возможности устройства и позволяют решать привычные задачи быстрее и проще.',
            'price' => 799.99,
            'image' => 'ipad-retina.jpg',
        ]);

        DB::table('products')->insert([
            'name' => 'Акустическая гитара',
            'slug' => 'acoustic-guitar',
            'description' => 'description goes here',
            'price' => 699.99,
            'image' => 'acoustic.jpg',
        ]);
        DB::table('products')->insert([
            'name' => 'Fender Stratocaster',
            'slug' => 'electric-guitar',
            'description' => 'Fender Stratocaster (или Strat) — модель электрогитары, разработанной Джорджем Фуллертоном, Лео Фендером и Фредди Таваресом в 1954 году и выпускаемой вплоть до настоящего времени.',
            'price' => 899.99,
            'image' => 'electric.jpg',
        ]);

        DB::table('products')->insert([
            'name' => 'Beats by Dre',
            'slug' => 'headphones',
            'description' => 'Beats by Dre — это мощный звук и самые современные технологии. Бесплатная доставка в течение 2 дней.',
            'price' => 99.99,
            'image' => 'headphones.jpg',
        ]);

        DB::table('products')->insert([
            'name' => 'Колонки',
            'slug' => 'speakers',
            'description' => 'Высококачественные динамики',
            'price' => 499.99,
            'image' => 'speakers.jpg',
        ]);
    }
}
